package com.example.demo.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.User;
import com.example.demo.repository.UserRepository;



@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;
	
	@Override
	public List<User> getUsers() {

		return (List<User>) userRepository.findAll();
	}
	
	@Override
	public User saveUser(User user) {

		return userRepository.save(user);
	}	

	@Override
	public Optional<User> getUsersById(String userId) {
		return userRepository.findById(userId);
	}

	@Override
	public User updateUser(String userId, User user) {
		User update=userRepository.findById(userId).orElse(null);
		update.setUserName(user.getUserName());
		update.setPassword(user.getPassword());
		update.setEmail(user.getEmail());
		return userRepository.save(update);
		
	}

	@Override
	public void deleteUser(String userId) {
		userRepository.deleteById(userId);
	}


	@Override
	public User getUser(String userId) {
		// TODO Auto-generated method stub
		return null;
	}


	
}