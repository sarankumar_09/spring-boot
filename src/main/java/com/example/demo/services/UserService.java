package com.example.demo.services;


import java.util.List;
import java.util.Optional;

import com.example.demo.User;


public interface UserService {
	User saveUser(User user);

	List<User> getUsers();
	
	public Optional<User> getUsersById(String userId);
   
	User getUser(String userId);
	
	void deleteUser(String userId);
	
	User updateUser(String userId,User user);

}