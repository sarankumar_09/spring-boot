package com.example.demo.controller;



import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.services.*;
import com.example.demo.User;

@RestController
@RequestMapping(value="/user")
public class UserController {
	
	@Autowired
	UserService userService;

	@GetMapping(value="/getAllUsers")
	public List<User> getUsers() {

		return userService.getUsers();
	}
	@GetMapping(value="/getUser/{userId}")
	public Optional<User> getUsersById(@PathVariable String userId) {

		return userService.getUsersById(userId);
	}
	
	@PostMapping(value="/saveUser")
	public User saveUser(@RequestBody User user ) {
		
		return userService.saveUser(user);
	}
	
	@DeleteMapping(value="/delUser/{userId}") 	
	public ResponseEntity<User> deleteUser(@PathVariable String userId) {
		userService.deleteUser(userId);
		
		return ResponseEntity.noContent().build();
	}
	
	@PutMapping(value="/UpdUser/{userId}") 	
	public User updateUser(@PathVariable String userId,@RequestBody User user) {
		
		return userService.updateUser(userId, user);
		
		 
	}
	
	
}